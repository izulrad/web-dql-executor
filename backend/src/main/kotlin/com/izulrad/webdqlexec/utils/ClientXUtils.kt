package com.izulrad.webdqlexec.utils

import com.documentum.com.DfClientX
import com.documentum.fc.client.*
import com.documentum.fc.common.IDfLoginInfo

import java.util.ArrayList

/**
 * Вспомогательный класс для работы с dql
 */
object ClientXUtils {

    val sessionManager: IDfSessionManager
        get() = DfClientX().localClient.newSessionManager()

    fun createLoginInfo(login: String, password: String): IDfLoginInfo {
        val loginInfo = DfClientX().loginInfo
        loginInfo.user = login
        loginInfo.password = password
        loginInfo.domain = null
        return loginInfo
    }

    val docbaseMap: IDfDocbaseMap
        get() = DfClientX().localClient.docbaseMap

    val docbases: List<String>
        get() {
            val docbases = ArrayList<String>()
            val docbaseMap = docbaseMap
            for (i in 0..docbaseMap.docbaseCount-1) {
                docbases.add(docbaseMap.getDocbaseName(i))
            }
            return docbases
        }

    fun createDql(dql: String): IDfQuery {
        val query = DfQuery()
        query.dql = dql
        return query
    }

    fun closeIDfCollection(collection: IDfCollection?) {
        collection?.close()
    }

    fun getIDfCollection(session: IDfSession, dql: String): IDfCollection {
        return ClientXUtils.createDql(dql).execute(session, DfQuery.DF_QUERY)
    }

    fun getString(typedObject: IDfTypedObject, name: String): String {
        return typedObject.getString(name)
    }

}
