package com.izulrad.webdqlexec

import com.documentum.fc.client.IDfCollection
import com.documentum.fc.client.IDfSessionManager
import com.documentum.fc.client.impl.typeddata.Attribute
import com.izulrad.webdqlexec.utils.ClientXUtils
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import java.util.*

@Component
open class DocumentumComponent {

    @Value("\${documentum.service_docbase}")
    private lateinit var serviceDocbase: String

    @Value("\${documentum.service_username}")
    private lateinit var serviceUsername: String

    @Value("\${documentum.service_password}")
    private lateinit var servicePassword: String

    val userSessionManager: IDfSessionManager
        @Bean
        @Qualifier("userSessionManager")
        @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
        get() = ClientXUtils.sessionManager

    val serviceSessionManager: IDfSessionManager
        @Bean
        @Qualifier("service")
        get() {
            val serviceSessionManager = ClientXUtils.sessionManager
            serviceSessionManager.setIdentity(serviceDocbase, ClientXUtils.createLoginInfo(serviceUsername, servicePassword))
            return serviceSessionManager
        }

    /**
     * Список аттрибутов dm_sysobject, служит для того чтобы скрывать его поля при запросе его потомков

     * @param serviceSessionManager менеджер сесиий
     * *
     * @return список аттрибутов скрываемых в запросах
     */
    @Bean
    open fun getHiddenAttrs(@Qualifier("service") serviceSessionManager: IDfSessionManager): Collection<String> {
        val session = serviceSessionManager.getSession(serviceDocbase)
        var c: IDfCollection? = null
        try {
            c = ClientXUtils.getIDfCollection(session, "SELECT * FROM dm_sysobject ENABLE (RETURN_TOP 0)")

            @Suppress("UNCHECKED_CAST")
            val hiddenAttrs = (c.enumAttrs() as Enumeration<Attribute>)
                    .toList()
                    .map { it.name }
                    .filter { a -> !NOT_HIDDEN_SYSOBJECT_ATTRS.contains(a) }
                    .toList()

            return hiddenAttrs + "dm_rnum"
        } finally {
            ClientXUtils.closeIDfCollection(c)
        }
    }

    companion object {

        /**
         * Список аттрибутов dm_sysobject которые не следует скрывать
         */
        private val NOT_HIDDEN_SYSOBJECT_ATTRS = setOf(
                "r_object_id",
                "object_name",
                "r_object_type",
                "title",
                "subject",
                "r_creation_date"
        )
    }
}