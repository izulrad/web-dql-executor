package com.izulrad.webdqlexec

import com.izulrad.webdqlexec.data.DqlRequest
import com.izulrad.webdqlexec.service.DqlService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * Контроллер для выполнения dql запросов
 */
@RestController
class Controller
@Autowired
constructor(private val dqlService: DqlService) {

    @CrossOrigin(origins = arrayOf("http://localhost:63342"))
    @PostMapping("/execute")
    fun execute(@RequestBody dqlRequest: DqlRequest) = dqlService.execute(dqlRequest)

    @CrossOrigin(origins = arrayOf("http://localhost:63342"))
    @GetMapping("/docbases")
    fun getDocbases() = dqlService.docbases

}
