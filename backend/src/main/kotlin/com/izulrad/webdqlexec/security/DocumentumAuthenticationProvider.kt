package com.izulrad.webdqlexec.security

import com.documentum.fc.client.IDfSessionManager
import com.izulrad.webdqlexec.utils.ClientXUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component

import java.util.Arrays

@Component
class DocumentumAuthenticationProvider
@Autowired
constructor(@Qualifier("userSessionManager") private val sessionManager: IDfSessionManager) : AuthenticationProvider {


    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication {
        val username = authentication.name
        val password = authentication.credentials as String

        val docbases = ClientXUtils.docbases
        for (docbase in docbases) {
            sessionManager.setIdentity(docbase, ClientXUtils.createLoginInfo(username, password))
        }
        for (docbase in docbases) {
            try {
                sessionManager.getSession(docbase)
            } catch (e: Exception) {
                for (toClear in docbases) sessionManager.clearIdentity(toClear)
                throw BadCredentialsException("Wrong login or password for base " + docbase, e)
            }

        }

        val roles = Arrays.asList<GrantedAuthority>(SimpleGrantedAuthority("ROLE_USER"))
        return UsernamePasswordAuthenticationToken(username, null, roles)
    }

    override fun supports(authentication: Class<*>): Boolean {
        return true
    }
}
