package com.izulrad.webdqlexec.security

import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

import javax.servlet.http.HttpSessionEvent
import javax.servlet.http.HttpSessionListener

@Component
class SecurityComponent {

    @Bean
    fun httpSessionListener(): HttpSessionListener {
        return object : HttpSessionListener {
            override fun sessionCreated(se: HttpSessionEvent) {
                //TODO если вдруг пользователей будет больше трех, надо бы переписать)
                se.session.maxInactiveInterval = 0
            }

            override fun sessionDestroyed(se: HttpSessionEvent) {
            }
        }
    }
}
