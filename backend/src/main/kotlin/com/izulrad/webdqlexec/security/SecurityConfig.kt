package com.izulrad.webdqlexec.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@EnableWebSecurity
open class SecurityConfig : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
                .antMatchers("/docbases").permitAll()
                .anyRequest().hasAnyRole("USER")
                .and().csrf().disable() //todo мб без отключения можно ангулярить?
                .formLogin()
    }

    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder,
                        documentumAuthenticationProvider: DocumentumAuthenticationProvider) {
        auth.authenticationProvider(documentumAuthenticationProvider)
    }
}
