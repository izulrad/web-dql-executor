package com.izulrad.webdqlexec.data

/**
 * Строка c данными в выборке, предсталвяет собой упорядоченный список строк
 * @see DqlResult
 */
data class DqlResultRow(val values: List<String>)
