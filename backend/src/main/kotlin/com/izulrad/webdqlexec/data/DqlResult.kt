package com.izulrad.webdqlexec.data

/**
 * Результат выборки, содержит информацию о аттрибутах выборки, ее строки и информацию о пагинации
 */
data class DqlResult(
    val leadTime: Int,
    val attributes: List<DqlResultAttribute>? = null,
    val rows: List<DqlResultRow>? = null,
    val pageable: DqlPageable? = null,
    val errorText: String? = null
)
