package com.izulrad.webdqlexec.data

/**
 * Информация о аттрибутах в результате выборки
 * @see DqlResult
 */
data class DqlResultAttribute(
        val index: Int,
        val name: String,
        val dataType: Int
)
