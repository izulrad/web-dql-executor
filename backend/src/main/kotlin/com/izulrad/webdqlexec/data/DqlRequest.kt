package com.izulrad.webdqlexec.data


/**
 * Запрос на выполнение DQL
 */
data class DqlRequest(
        val dql: String,
        val docbase: String,
        val quickSearch: Boolean,
        val returnTop: Int,
        val pageable: DqlPageable?
) {
    constructor() : this("", "", false, 10, null)
}