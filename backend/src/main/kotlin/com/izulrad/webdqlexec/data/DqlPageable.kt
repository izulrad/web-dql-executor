package com.izulrad.webdqlexec.data

/**
 * Информация о пагинации, используется в DqlResult и DqlRequest
 * @see DqlResult
 * @see DqlRequest
 */
class DqlPageable {
    var totalItems: Int = 0
    var currentPage: Int = 1
    var perPage: Int = 10
    var orders: List<String>? = null

    constructor()

    constructor(totalItems: Int, currentPage: Int, perPage: Int, orders: List<String>?) {
        this.totalItems = totalItems
        this.currentPage = currentPage
        this.perPage = perPage
        this.orders = orders
    }
}