package com.izulrad.webdqlexec

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer

//@SpringBootApplication //WAR
//open class WebDqlExecApplication: SpringBootServletInitializer(){
//
//    override fun configure(builder: SpringApplicationBuilder?): SpringApplicationBuilder {
//        return builder!!.sources(WebDqlExecApplication::class.java);
//    }
//}

@SpringBootApplication //JAR
open class WebDqlExecApplication

fun main(args: Array<String>) {
    SpringApplication.run(WebDqlExecApplication::class.java, *args)
}