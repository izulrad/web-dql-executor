package com.izulrad.webdqlexec.service

import com.documentum.fc.client.IDfCollection
import com.documentum.fc.client.IDfSessionManager
import com.documentum.fc.client.impl.typeddata.Attribute
import com.izulrad.webdqlexec.data.*
import com.izulrad.webdqlexec.utils.ClientXUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import java.util.*

/**
 * Сервис для выполнения dql запросов
 */
@Service
class DqlService
@Autowired
constructor(@Qualifier("userSessionManager")
            private val sessionManager: IDfSessionManager,
            private val hiddenAttrs: Collection<String>) {

    /**
     * Выполняет переданный запрос

     * @param dqlRequest запрос для выполнения
     * *
     * @return результат выполнения запроса
     */
    fun execute(dqlRequest: DqlRequest): DqlResult {
        val start = System.currentTimeMillis()
        var c: IDfCollection? = null
        var dql = dqlRequest.dql.trim()

        try {
            val session = sessionManager.getSession(dqlRequest.docbase)

            c = ClientXUtils.getIDfCollection(session, dql)

            val pageable: DqlPageable
            if (dql.toLowerCase().startsWith("select")) {
                //запрос на выборку
                if (dqlRequest.quickSearch) {
                    pageable = DqlPageable(dqlRequest.returnTop, 1, dqlRequest.returnTop, null)
                    //если в запросе есть ENABLE, то используем имеющийся для хинтов
                    val myEnableGroup = "RETURN_TOP ${dqlRequest.returnTop}, OPTIMIZE_TOP ${dqlRequest.returnTop}"
                    val m = Regex("enable\\s?\\((.*)\\)\\s?\$").find(dql.toLowerCase()) //todo небезопасный паттерн, может лишка выбрать если поле будет называться enable например
                    if (m != null) {
                        val beforeEnable = dql.substring(0..m.range.start - 1)
                        val userEnableGroup = m.groupValues[1]
                        dql = "$beforeEnable ENABLE($userEnableGroup, $myEnableGroup)"
                    } else {
                        dql += " ENABLE($myEnableGroup)"
                    }
                } else {
                    pageable = generatePageable(dqlRequest)
                }
            } else {
                //служебный запрос
                pageable = DqlPageable(1, 1, 10, null)
            }

            @Suppress("UNCHECKED_CAST")
            var attrs = (c.enumAttrs() as Enumeration<Attribute>).toList()
            if (dql.contains("*")) attrs = attrs.filter { !hiddenAttrs.contains(it.name) }
            val dqlResultAttributes = attrs.map { DqlResultAttribute(it.index, it.name, it.dataType) }

            val firstItem = pageable.currentPage * pageable.perPage - pageable.perPage
            for (i in 1..firstItem) c.next()

            val dqlResultRows = ArrayList<DqlResultRow>(pageable.perPage)
            for (i in 1..pageable.perPage) {
                if (c.next()) {
                    val typedObject = c.typedObject
                    val row = dqlResultAttributes.map { ClientXUtils.getString(typedObject, it.name) }
                    dqlResultRows.add(DqlResultRow(row))
                } else break
            }

            System.out.printf("\nfor user: %s \n docbase: %s \n  dql: %s\n",
                    session.loginUserName, dqlRequest.docbase, dql)

            val timeLead = System.currentTimeMillis() - start;
            return DqlResult(timeLead.toInt(), dqlResultAttributes, dqlResultRows, pageable)
        } catch (e: Exception) {
            return DqlResult(leadTime = (System.currentTimeMillis() - start).toInt(), errorText = e.message)
        } finally {
            ClientXUtils.closeIDfCollection(c)
        }
    }

    /**
     * Генерирует объект пагинации, на основе запроса

     * @param dqlRequest запрос
     * *
     * @return новый объект DqlPageable
     */
    private fun generatePageable(dqlRequest: DqlRequest): DqlPageable {
        val pageable = dqlRequest.pageable ?: DqlPageable()
        val dql = dqlRequest.dql
        val lowerCaseDql = dql.toLowerCase()

        val mReturnTop = Regex("return_top\\s?(\\d+)").find(lowerCaseDql) //todo уточнить паттерн он должен быть в enable и все такое
        if (mReturnTop != null) {
            pageable.totalItems = mReturnTop.groupValues[1].toInt()
        } else {
            var c: IDfCollection? = null
            val dqlForCount = if (lowerCaseDql.contains("order by"))
                dql.substring(0, lowerCaseDql.indexOf("order by"))
            else if (lowerCaseDql.contains("enable"))
                dql.substring(0, lowerCaseDql.indexOf("enable"))
            else dql

            try {
                val session = sessionManager.getSession(dqlRequest.docbase)
                val selectTotalItems = "SELECT COUNT(*) AS total_items FROM ($dqlForCount)"
                c = ClientXUtils.getIDfCollection(session, selectTotalItems)
                if (c.next()) {
                    pageable.totalItems = c.typedObject.getInt("total_items")
                }
            } catch (e: Exception) {
                var s = e.message
                if (e.message?.contains("syntax error") ?: false) {
                    s = "[DM_QUERY_E_SYNTAX]error: \"A Parser Error (syntax error) has occurred in the vicinity of: $dql\""
                }
                throw Exception(s)
            } finally {
                ClientXUtils.closeIDfCollection(c)
            }
        }

        //todo эта шняга не отдается если быстрый поиск... перенести из execute в этот метод логику проверки быстрого поиска
        val mOrder = Regex("\\s+order\\s+by\\s+((\\s?[\\w]+(\\s+asc|\\s+desc)?,)*(\\s?[\\w]+(\\s+asc|\\s+desc)?))")
                .find(lowerCaseDql)
        if (mOrder!=null) {
            pageable.orders = mOrder.groupValues[1].split(",").map(String::trim)
        }

        return pageable;
    }

    val docbases: List<String>
        get() = ClientXUtils.docbases
}
