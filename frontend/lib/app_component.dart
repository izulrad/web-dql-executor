import 'package:angular2/core.dart';
import 'package:frontend/dql/dql_input/dql_input_component.dart';
import 'package:frontend/dql/dql_service.dart';
import 'package:frontend/dql/dql_template_service.dart';

@Component(
    selector: 'my-app',
    styleUrls: const ['app_component.css'],
    templateUrl: 'app_component.html',
    directives: const [DqlInputComponent],
    providers: const [
      DqlService,
      DqlTemplateService
    ]
)
class AppComponent {}
