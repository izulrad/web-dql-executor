import 'dart:async';
import 'dart:convert';

import 'package:angular2/core.dart';
import 'package:frontend/dql/data/dql_pageable.dart';
import 'package:frontend/dql/data/dql_request.dart';
import 'package:frontend/dql/data/dql_result.dart';
import 'package:http/browser_client.dart';
import 'package:http/http.dart';

@Injectable()
class DqlService {
//  static const _executeUrl = 'http://localhost:8888/execute'; //TODO ifDEBUG
//  static const _docbasesUrl = 'http://localhost:8888/docbases'; //TODO ifDEBUG
  static const _executeUrl = 'execute';
  static const _docbasesUrl = 'docbases';

  final BrowserClient _http;

  DqlService(this._http);

  Future<DqlResult> executeQuery(DqlRequest request) async {
    final Response response =
    await _http.post(_executeUrl,
        headers: {'Content-Type': 'application/json'},
        body: JSON.encode(request.toJSON()));
    return new DqlResult.fromJSON(JSON.decode(response.body));
  }

  Future<List<String>> getDocbases() async{
    final Response response =
        await _http.get(_docbasesUrl);
    return JSON.decode(response.body);
  }
}