import 'dart:async';

import 'package:angular2/core.dart';
import 'package:frontend/dql/data/dql_template.dart';
import 'package:frontend/dql/dql_template_service.dart';

@Component(
    selector: 'dql-templates',
    templateUrl: 'dql_templates_component.html',
    styleUrls: const['dql_templates_component.css']
)
class DqlTemplatesComponent implements OnInit {

  @Output()
  EventEmitter<String> executeDql = new EventEmitter<String>();

  @Output()
  EventEmitter<String> setDql =  new EventEmitter<String>();

  @ViewChild("modal")
  ElementRef modal;

  List<DqlTemplate> templates;
  DqlTemplate selectedTemplate;

  final DqlTemplateService _dqlTemplateService;

  DqlTemplatesComponent(this._dqlTemplateService);

  @override
  ngOnInit() async {
    templates = await _dqlTemplateService.getTemplates();
  }

  void selectTemplate(DqlTemplate template){
    selectedTemplate = template;
  }

  void executeTemplate(DqlTemplate tmp) {
    executeDql.emit(tmp.params != null ? _parameterizeDql(tmp) : tmp.dql);
    closeDiv(modal.nativeElement);
  }

  String _parameterizeDql(DqlTemplate tmp) {
    String dql = tmp.dql;
    tmp.params
        .map((p) => {'name':p.name, 'value':p.value})
        .forEach((p) => dql = dql.replaceAll("{{${p['name']}}}", p['value'] ?? p['name']));
    return dql;
  }

  void copyDql(DqlTemplate tmp){
    setDql.emit(tmp.params != null ? _parameterizeDql(tmp) : tmp.dql);
    closeDiv(modal.nativeElement);
  }

  void showDiv(dynamic div){
    div.style.display = "block";
  }
  void closeDiv(dynamic div){
    div.style.display = "none";
  }
}