import 'package:angular2/core.dart';
import 'package:frontend/dql/data/dql_result.dart';

@Component(
    selector: 'dql-result',
    templateUrl: 'dql_result_component.html',
    styleUrls: const['dql_result_component.css']
)
class DqlResultComponent {

  @Input()
  DqlResult dqlResult;

}