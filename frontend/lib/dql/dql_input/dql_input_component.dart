import 'dart:async';

import 'package:angular2/core.dart';
import 'package:frontend/dql/data/dql_pageable.dart';
import 'package:frontend/dql/data/dql_request.dart';
import 'package:frontend/dql/data/dql_result.dart';
import 'package:frontend/dql/dql_pagination/dql_pagination_component.dart';
import 'package:frontend/dql/dql_result/dql_result_component.dart';
import 'package:frontend/dql/dql_service.dart';
import 'package:frontend/dql/dql_templates/dql_templates_component.dart';
import 'package:html_unescape/html_unescape.dart';

@Component(
    selector: 'dql-input',
    templateUrl: 'dql_input_component.html',
    styleUrls: const ['dql_input_component.css'],
    directives: const [
      DqlResultComponent,
      DqlTemplatesComponent,
      DqlPaginationComponent
    ]
)
class DqlInputComponent implements OnInit {

  DqlResult dqlResult;
  DqlRequest dqlRequest;
  DqlRequest _lastDqlRequest;

  List<String> docbases;
  String currentDocbase;
  bool isExecuting = false;
  bool quickSearch = true;
  int perPage = 10;
  int maxPerPage = 30;

  String dqlQuery;

  @ViewChild("dta")
  ElementRef dqlTextArea;

  final DqlService _dqlService;

  DqlInputComponent(this._dqlService);

  void executeNewSelectedQuery(dynamic ta) {
    if (ta.selectionStart == ta.selectionEnd) {
      executeNewQuery(dqlQuery);
    } else {
      executeNewQuery(ta.value.substring(ta.selectionStart, ta.selectionEnd));
    }
  }

  void executeNewQuery(String dql) {
    if (dql == null) return;
    dqlResult = null;
    if (quickSearch) {
      dqlRequest = new DqlRequest(dql);
      dqlRequest.quickSearch = true;
      dqlRequest.returnTop = perPage;
    } else {
      dqlRequest = new DqlRequest(dql);
      dqlRequest.pageable = new DqlPageable(0, 1, perPage, null);
    }
    dqlRequest.docbase = currentDocbase;
    executeQuery(dqlRequest);
  }

  void selectPage(int page) {
    _lastDqlRequest.pageable.currentPage = page;
    executeQuery(_lastDqlRequest);
  }

  Future<Null> executeQuery(DqlRequest request) async {
    isExecuting = true;
    dqlResult = await _dqlService.executeQuery(request);
    request.pageable = dqlResult.pageable;
    _lastDqlRequest = request;
    isExecuting = false;
  }

  void unescapeQuery() {
    dqlRequest.dql = new HtmlUnescape().convert(dqlRequest.dql);
  }

  void setDql(String dql) {
    dqlQuery = dql;
  }

  void perPageChange(dynamic event) {
    if (perPage > maxPerPage)
      perPage = maxPerPage;
    else if (perPage < 1) perPage = 1;
  }

  @override
  ngOnInit() async {
    docbases = await _dqlService.getDocbases();
    currentDocbase = docbases[0];
  }
}