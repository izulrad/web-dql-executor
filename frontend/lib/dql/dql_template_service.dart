import 'dart:async';
import 'package:angular2/core.dart';
import 'package:frontend/dql/data/dql_template.dart';
import 'package:http/browser_client.dart';


@Injectable()
class DqlTemplateService {
  static const _serviceUrl = '/dql-template';

  final BrowserClient _http;

  DqlTemplateService(this._http);

  Future<List<DqlTemplate>> getTemplates() async {
    return await DqlTemplate.mock();
  }
}