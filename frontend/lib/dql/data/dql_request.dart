import 'package:frontend/dql/data/dql_pageable.dart';

class DqlRequest {
  DqlPageable pageable;
  String dql;
  bool quickSearch;
  int returnTop;
  String docbase;

  DqlRequest(this.dql);

  Map<String, dynamic> toJSON() =>
      {
        'pageable': pageable?.toJSON(),
        'dql':dql,
        'quickSearch':quickSearch,
        'returnTop':returnTop,
        'docbase':docbase
      };

}