import 'package:frontend/dql/data/dql_template_param.dart';

class DqlTemplate {
  final String name;
  final String dql;
  final String description;
  final List<DqlTemplateParam> params;

  DqlTemplate(this.name, this.dql, this.description, this.params);

  factory DqlTemplate.fromJSON(Map<String, dynamic> dqlTemplate) =>
      new DqlTemplate(dqlTemplate['name'], dqlTemplate['dql'],
          dqlTemplate['description'], dqlTemplate['params']
              .map((p) => new DqlTemplateParam.fromJSON(p))
              .toList());


  Map<String, dynamic> toJSON() =>
      {
        'dql':dql,
        'params': params
      };

  static List<DqlTemplate> mock() =>
      [//TODO отдельно в базу
        new DqlTemplate(
            'Group by username',
            "SELECT DISTINCT * FROM dm_group WHERE ANY users_names = '{{user_name}}'",
            'Find all user group by user name',
            [new DqlTemplateParam('user_name', 'User name')])
        , new DqlTemplate(
            'Usernames by group',
            "SELECT i_all_users_names FROM dm_group WHERE group_name = '{{group_name}}'",
            'Find all users by group name',
            [new DqlTemplateParam('group_name', 'Group name')])
        , new DqlTemplate(
            "Need more resolutions",
            """
            SELECT count(*), d.r_object_type, d.system_number, d.owner_name
            FROM dt_instruction i
            	LEFT JOIN dt_execution e ON i.executable = e.r_object_id
            	LEFT JOIN dt_document d ON e."document" = d.r_object_id
            GROUP BY i.executable, d.r_object_type, d.system_number, d.owner_name
            ORDER BY 1 DESC
            """,
            'Find document with greater count of resolutions',
            null)
        , new DqlTemplate(
            "Need more negotiations",
            """
            SELECT count(*), d.r_object_type, d.system_number, d.owner_name
            FROM dt_negotiation_list nl
            	LEFT JOIN dt_negotiable_document nd ON nd.negotiable_document = nl.negotiable_document
            	LEFT JOIN dt_document d ON nd."document" = d.r_object_id
            GROUP BY nl.negotiable_document, nd.negotiable_document, nd."document", d.r_object_type, d.system_number, d.owner_name
            ORDER BY 1 DESC
            """,
            'Find document with greater count of negotiations',
            null)
        , new DqlTemplate(
            'Set permisson by type',
            "exec do_method with method='set_permissions', arguments='-docbase_name {{docbase_name}} -document_type {{document_type}}'",
            'Set permission to all objects of specific type in specified base',
            [
              new DqlTemplateParam('docbase_name', 'Docbase name')
              , new DqlTemplateParam('document_type', 'Document type')
            ])
        , new DqlTemplate(
            'Set permisson by document id',
            "exec do_method with method='set_permissions', arguments='-docbase_name {{docbase_name}} -document_id {{document_id}}'",
            'Set permission to object by id in specified base',
            [
              new DqlTemplateParam('docbase_name', 'Docbase name')
              , new DqlTemplateParam('document_id', 'Document id')
            ])
      ];
}