class DqlResultAttribute{

  final int index;
  final String name;
  final int dataType;

  DqlResultAttribute(this.index, this.name, this.dataType);

  factory DqlResultAttribute.fromJSON(Map<String, dynamic> attribute) =>
      new DqlResultAttribute(
          attribute['index'],
          attribute['name'],
          attribute['dataType']
      );

}
