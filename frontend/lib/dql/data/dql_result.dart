import 'package:frontend/dql/data/dql_pageable.dart';
import 'package:frontend/dql/data/dql_result_attribute.dart';
import 'package:frontend/dql/data/dql_result_row.dart';

class DqlResult {
  final List<DqlResultAttribute> attributes;
  final List<DqlResultRow> rows;
  final DqlPageable pageable;
  final int leadTime;
  final String errorText;

  DqlResult(this.attributes, this.rows, this.pageable, this.leadTime, this.errorText);

  factory DqlResult.fromJSON(Map<String, dynamic> dqlResult) {
    DqlResult result;

    if (dqlResult['errorText'] != null) {
      result = new DqlResult(null, null, null, dqlResult['leadTime'], dqlResult['errorText']);
    } else {
      List<DqlResultAttribute> _attrs = dqlResult['attributes']
          .map((a) => new DqlResultAttribute.fromJSON(a)).toList();

      List<DqlResultRow> _rows = dqlResult['rows']
          .map((r) => new DqlResultRow(r['values'])).toList();

      DqlPageable _pageable = new DqlPageable.fromJSON(dqlResult['pageable']);

      result = new DqlResult(_attrs, _rows, _pageable, dqlResult['leadTime'], null);
    }

    return result;
  }

}