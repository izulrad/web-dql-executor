class DqlPageable {
  final int totalItems;
  int currentPage;
  final int perPage;
  final List<String> orders;

  DqlPageable(this.totalItems, this.currentPage, this.perPage, this.orders);

  factory DqlPageable.fromJSON(Map<String, dynamic> pageable) =>
      new DqlPageable(
          pageable['totalItems'], pageable['currentPage'],
          pageable['perPage'], pageable['orders']
      );

  Map<String, dynamic> toJSON() =>
      {
        'totalItems':totalItems,
        'currentPage':currentPage,
        'perPage':perPage,
        'orders':orders
      };

}