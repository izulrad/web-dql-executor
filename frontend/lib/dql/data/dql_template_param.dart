class DqlTemplateParam {
  final String name;
  final String description;
  String value;

  DqlTemplateParam(this.name, this.description);

  factory DqlTemplateParam.fromJSON(Map<String, dynamic> param){
    return new DqlTemplateParam(param['name'], param['description']);
  }

  Map<String, dynamic> toJSON() =>
      {
        'name': name,
        'description': description
      };
}