import 'package:angular2/core.dart';
import 'package:frontend/dql/data/dql_pageable.dart';

@Component(
    selector: 'dql-pagination',
    templateUrl: 'dql_pagination_component.html',
    styleUrls: const ['dql_pagination_component.css'],
)
class DqlPaginationComponent{

  @Input()
  DqlPageable pageable;

  @Output()
  EventEmitter<int> selectPage = new EventEmitter<int>();

  void changePage(int page){
    selectPage.emit(page);
  }

  /**
   * Возвращает список номеров страниц, для пагинации
   * @param currentPage - текущая страница
   * @param totalPages - всего страниц
   * @param range(@default = 2) - кол-во тображаемых страниц по бокам текущей,
   *        т.е. при значени 2 будет 2 страницы слева и 2 справа от текущей
   *        2(слева) + 1(текущая страница) + 2(справа) итого 5(range*2+1)
   */
  List<int> pages(int currentPage, int totalPages, [int range = 2]) {
    int pageCount = range * 2 + 1;
    if (pageCount > totalPages) pageCount = totalPages;

    int delta;
    if (currentPage - range <= 0) {
      delta = 1;
    } else if (currentPage + range > totalPages) {
      delta = totalPages - pageCount + 1;
    } else {
      delta = currentPage - range;
    }

    return new List<int>.generate(pageCount, (i) => i + delta);
  }
}