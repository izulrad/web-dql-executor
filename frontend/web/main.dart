import 'package:angular2/core.dart';
import 'package:angular2/platform/browser.dart';
import 'package:frontend/app_component.dart';
import 'package:http/browser_client.dart';

main() {
  bootstrap(AppComponent, [
    [provide(BrowserClient, useFactory: () => new BrowserClient(), deps: [])]
  ]);
}
